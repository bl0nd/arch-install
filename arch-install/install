#!/usr/bin/env bash

# >> Helper Functions

color() {
  ###########################################################
  # Print task statuses.
  #
  #
  # Args:
  #   $1: (String) A letter indicating which status to use.
  #   $2: (String) The message to color.
  #
  # Returns:
  #   A String that's colored.
  ###########################################################
  red="$(tput setaf 1; tput bold)"
  green="$(tput setaf 2; tput bold)"
  yellow="$(tput setaf 3; tput bold)"
  cyan="$(tput setaf 6; tput bold)"
  reset="$(tput sgr0)"

  if [[ "$1" == "r" ]]; then
    printf "${red}$2${reset}"
  elif [[ "$1" == "g" ]]; then
    printf "${green}$2${reset}"
  elif [[ "$1" == "y" ]]; then
    printf "${yellow}$2${reset}"
  elif [[ "$1" == "c" ]]; then
    printf "${cyan}$2${reset}"
  fi
}

status() {
  ###########################################################
  # Print status messages.
  #
  # Mappings:
  #   e (empty):  [      ]
  #   o (ok):     [  OK  ]
  #   n (no):     [  NO  ]
  #   f (Failed): [FAILED]
  #
  # Args:
  #   $1: (String) A letter indicating which status to use.
  #   $2: (String) The message to be displayed
  #
  # Returns:
  #   A String displaying a task and its current status.
  ###########################################################
  if [[ "$1" == "e" ]]; then
    printf "[      ]: $2\n"
  elif [[ "$1" == "o" ]]; then
    printf "[  $(color g "OK")  ]: $2\n"
  elif [[ "$1" == "n" ]]; then
    printf "[  $(color r "NO")  ]: $2\n"
  elif [[ "$1" == "f" ]]; then
    printf "[$(color r "FAILED")]: $2\n"
  fi
}

clearln() {
  ###########################################################
  # Clear a specified number of lines.
  #
  # Args:
  #   $1: (int) Number of lines to clear.
  #
  # Returns:
  #   None
  ###########################################################
  for i in $(seq $1); do
    tput cuu1
    tput el
  done
}

null() {
  ###########################################################
  # Redirects command output and error messages to /dev/null.
  #
  # Args:
  #   $@: (command) The command to run
  ###########################################################
  "$@" > /dev/null 2>&1
}

reprompt() {
  ###########################################################
  # Output starting and status prompts that were cleared.
  #
  # Global:
  #   volgroup: (String) The volume group name.

  # Args:
  #   $1: (int) The number of statuses to print.
  #
  # Returns:
  #   A String containing the starting prompt and a specified
  #     number of status messages.
  ###########################################################
  prompts=("Verified EFI Boot Mode."
           "Established Network Connection."
           "Created Partitions."
           "Created Volume Group '${volgroup}'."
           "Created Logical Volume 'root'."
           "Created Logical Volume 'home'."
           "Formatted Volumes."
           "Mounted Filesystems."
           )
  printf "Starting $(color c "Arch") Installation Script...\n\n"
  for i in "${prompts[@]:0:$1}"; do
    status o "$i"
  done
}

list_partitions() {
  ###########################################################
  # List all existing partitions on `device`.
  #
  # It's used to help tell which partitions are newly made
  #   when multi-booting to avoid hardcoding the partition
  #   variables (`esp_part`, `boot_part`, `lvm_part``).
  #
  # Since the partition table may be empty, we have to check
  # and see if there are existing partitions before we add
  # old partitions to our String.
  #
  # Args:
  #   $1: (String) Device name (e.g., sda, sdb, etc.).
  #
  # sReturns:
  #   A String containing newline-delimited partition labels.
  ###########################################################
  lsblk -J | python -c "\
import sys, json
partitions = ''
blockdevices = json.load(sys.stdin)['blockdevices']
for block in blockdevices:
  if block.get('name') == sys.argv[1]:
    if block.get('children'):
      for partition in block.get('children'):
        partitions += partition.get('name') + '\n'
print(partitions)" $1
}


# >> Install Functions

verify_efi() {
  if [[ "$(ls /sys/firmware/efi/efivars 2>/dev/null)" ]]; then
    status o "Verified EFI Boot Mode."
  else
    status n "Verified EFI Boot Mode."
    exit 1
  fi
}

connectivity() {
  status e "Setting up Network Connection..."

  # >>> Test Network Connection

  if [[ "$(null ping -c 1 8.8.8.8; echo $?)" == 0 ]]; then
    clearln 1
    status o "Established Network Connection."

  # >>> Select Network Connection Type

  else
    printf "\n\t  [1] Wireless\n\t  [2] Wired\n"
    while true; do
      printf "\n\t  Enter network connection type: "
      read c_type
      if [[ "${c_type}" != 1 && "${c_type}" != 2 ]]; then
        printf "\n\t  $(color r "Invalid connection type.")\n" >&2
        sleep 2
        clearln 4

  # >>> Select Network Name

      else
        echo
        ip link show
        printf "\n\t  $(color y "Enter the network interface name: ")"
        read interface

  # >>> Wireless Configuration

        if [[ "${c_type}" == 1 ]]; then
          printf "\n\t  "  # wifi-menu prompts 'scanning for networks...'
          wifi-menu -o 2>/dev/null
          reprompt 1
          status e "Establishing Network Connections..."
          break

  # >>> Wired Configuration

        elif [[ "${c_type}" == 2 ]]; then
          null ip link set "${interface}" up
          null dhcpcd "${interface}"
          break
        fi
      fi
    done
    sleep 10  # Let dhclient work a bit
    clearln 1

  # >>> Test Connection Again

    if [[ "$(null ping -c 1 8.8.8.8; echo $?)" == 0 ]]; then
      status o "Established Network Connection."
    else
      status n "Established Network Connection."
      exit 1
    fi
  fi
}

partitions() {
  #  Need to modify for SSD (partition labels are pX e.g., nvme0np1)
  status e "Creating Partitions...\n"
  lsblk

  # >>> Check for PGs, VGs, and LVs

  if [[ "$(lvdisplay)" || "$(pvdisplay)" || "$(vgdisplay)" ]]; then
    printf "\
    \n\t  $(color r "WARNING:") Make sure all volumes and groups are deleted.
    \t\t   Use $(color y "(lv|vg|pv)display") & $(color y "(lv|vg|pv)remove").\n\n"
    exit 1
  fi

  # >>> Select Device to Partition

  printf "\n\t  Enter device to partition: "
  read device
  if [[ "${device}" == nvme* ]]; then
    ssd_check=true  # For mkinitcpio in chroot
    prefix="p"
  fi

  # >>>> Snapshot of Existing Partitions

  start_parts="$(list_partitions "${device}")"

  # >>> Multi-Boot Checks

  printf "\n\t  Are you multi-booting [y/n]: "
  read multiboot

  if [[ "${multiboot}" == "y" ]]; then
    
    # >>>> Space Check

    bytes="$(gdisk -l /dev/${device} 2>/dev/null | grep Total | grep "[GM]iB")"
    space_left="$(gdisk -l /dev/${device} 2>/dev/null | grep -o "([0-9].*)" | \
	tr -d "()")"
    if [[ "${bytes}" == "MiB" ]]; then
      printf "$(color r "WARNING: Not enough space (${space_left} ${bytes} left)")"
      exit 1
    fi
    printf "\
    \n\t  $(color r "WARNING:") Make sure there is at least 1 GB available for
    \t\t   the EFI system partition and potential /boot
    \t\t   partition ($(color y "${space_left}") left)\n"

    # >>>> ESP Check

    esp_line="$(gdisk -l /dev/${device} 2>/dev/null | grep EF00)"
    if [[ "${esp_line}" ]]; then
      esp_check=true
      # the extra space at the start forcing us to use -f2 isn't there
      # nvm it's back
      esp_num="$(echo "${esp_line}" | tr -s " " | cut -d " " -f2)"
      esp_part="${device}${prefix}${esp_num}"
    fi
  fi

  # >>> Boot Loader Selection

  printf "\n\t  [1] GRUB\n\t  [2] rEFInd\n"
  while true; do
    printf "\n\t  Enter boot loader: "
    read loader

  # >>> Partition Disk

    if [[ "${loader}" == 1 || "${loader}" == 2 ]]; then
      lsblk_length="$(lsblk | wc -l)"

      (

      # >>>> New GPT & ESP

      if [[ "${multiboot}" == "n" ]]; then
        echo o
        echo y

        echo n
        echo
        echo
        echo +300M
        echo ef00
      fi

      # >>>> Boot (Linux Filesystem)

      if [[ "${loader}" == 1 ]]; then
	      echo n
        echo
        echo
        echo +400M
        echo
      fi

      # >>>> Swap
      # echo n
      # echo
      # echo
      # echo +${swap_space}G
      # echo SWAP_TYPE

      # >>>> LVM Partition

      echo n
      echo
      echo
      echo
      echo 8e00

      # >>>> Write
      
      echo w
      echo y
      ) | null gdisk /dev/"${device}"
      break
    else
      printf "\n\t  $(color r "Invalid boot loader choice.")\n" >&2
      sleep 2
      clearln 4
    fi
  done

  # >>>> Rescan Devices

  null partprobe /dev/"${device}"

  # >>> Assign Partition Values

  if [[ "${multiboot}" == "y" ]]; then
    
    # >>>> Partition Setup

    new_parts="$(list_partitions "${device}")"
    # Okay, we convert all \n to ' ' with tr so that we can easily cut later
    # To make this fit, we can use start_parts and new_parts
    part_diff="$(diff -w <(printf "${start_parts}") <(printf "${new_parts}"))"
    difference="$(echo "${part_diff}" | grep -o "${device}.*" | tr "\n" " ")"

    # >>>> GRUB

    if [[ "${loader}" == 1 ]]; then
      if [[ "${esp_check}" ]]; then
	boot_part="$(echo "${difference}" | cut -d " " -f1)"
	lvm_part="$(echo "${difference}" | cut -d " " -f2)"
      else
	boot_part="$(echo "${difference}" | cut -d " " -f2)"
	lvm_part="$(echo "${difference}" | cut -d " " -f3)"
      fi
    else
      if [[ "${esp_check}" ]]; then
	lvm_part="$(echo "${difference}" | cut -d " " -f1)"
      else
	lvm_part="$(echo "${difference}" | cut -d " " -f2)"
      fi
    fi

    # >>>> rEFInd

  else
    esp_part="${device}${prefix}1"
    if [[ "${loader}" == 1 ]]; then
      boot_part="${device}${prefix}2"
      lvm_part="${device}${prefix}3"
    else
      lvm_part="${device}${prefix}2"
    fi
  fi

  clearln "${lsblk_length}"
  if [[ "${multiboot}" == "y" ]]; then
    clearln 15
  else
    clearln 11
  fi
  status o "Created Partitions."
}

create_vg() {
  status e "Creating Volume Group..."
  printf "\n\t  Enter volume group name: "
  read volgroup
  null vgcreate -f --dataalignment 1m "${volgroup}" /dev/"${lvm_part}"
  clearln 3
  status o "Created Volume Group '${volgroup}'."

}

create_lv() {
  status e "Creating Logical Volume 'root'..."

  # >>> Create Logical Volumes

  printf "\n\t  Enter amount of 'root' space (GiB): "
  read root_size
  null lvcreate -y -L "${root_size}"G "${volgroup}" -n root
  clearln 3
  status o "Created Logical Volume 'root'."

  status e "Creating Logical Volume 'home'..."
  null lvcreate -y -l +100%FREE "${volgroup}" -n home

  # >>> Activate Logical Volumes

  # Note: It seems to work without all this so...
  modprobe dm_mod
  null vgscan
  null vgchange -ay
  clearln 1
  status o "Created Logical Volume 'home'."
}

format_vol() {
  status e "Formatting Volumes..."
  #if [[ ! "${esp_check}" ]]; then
  if [[ "${multiboot}" == "n" ]]; then
    null mkfs.vfat -F32 /dev/"${esp_part}"
  fi
  if [[ "${loader}" == 1 ]]; then
    null mkfs.ext4 /dev/"${boot_part}"
  fi
  null mkfs.ext4 /dev/"${volgroup}"/root
  null mkfs.ext4 /dev/"${volgroup}"/home
  clearln 1
  status o "Formatted Volumes."
}

mount_fs() {
  status e "Mounting Filesystems..."
  mount /dev/"${volgroup}"/root /mnt
  mkdir /mnt/{boot,home}
  if [[ "${loader}" == 1 ]]; then
    mount /dev/"${boot_part}" /mnt/boot
  elif [[ "${loader}" == 2 ]]; then
    mount /dev/"${esp_part}" /mnt/boot
  fi
  mount /dev/"${volgroup}"/home /mnt/home
  clearln 1
  status o "Mounted Filesystems."
}

update_mirrors() {
  status e "Updating & Selecting Mirrors..."

  # >>> Allow 32/64-bit Installations

  cp /etc/pacman.conf /etc/pacman.conf.backup
  sed -i "92,95s/#\[multilib\]/\[multilib\]/" /etc/pacman.conf
  sed -i "92,95s/#Include/Include/" /etc/pacman.conf
  cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
  sleep 5  # So people can see the status message before vim

  # >>> Select Mirrors

  vim /etc/pacman.d/mirrorlist
  clear
  reprompt 9
  status o "Updated & Selected Mirrors."
}

do_pacstrap() {
  status e "Installing Base Packages..."
  null pacman -Syy
  null pacstrap /mnt base base-devel gvim iw wpa_supplicant dialog dhclient \
  linux-headers linux-lts linux-lts-headers efibootmgr os-prober git
  clearln 2
  status o "Installed Base Packages."
}

gen_fstab() {
  # The fstab file has legacy "fsck sequence" fields that order and parallelize
  # filesystem checks that make sure filesystem data structures are consistent.
  # Now that fscks are fast, the only thing that matters is that the root
  # fs is checked first.
  status e "Generating fstab..."
  genfstab -U -p /mnt >> /mnt/etc/fstab
  clearln 1
  status o "Generated fstab."
}

do_chroot() {
  status e "Chrooting into System..."
  cp ~/arch-install/arch-install/chroot /mnt/chroot
  cp ~/arch-install/arch-install/post /mnt/post
  clearln 1
  status o "Chrooted into System."

  # >>> Export Variables Needed By Chroot Script

  export loader
  if [[ "${multiboot}" == "y" ]]; then
    export multiboot
  fi
  export esp_part
  if [[ "${esp_check}" ]]; then
    export esp_check
  fi
  export volgroup

  # >>> Chroot

  arch-chroot /mnt ./chroot
  rm -r /mnt/chroot
}

do_reboot() {
  status e "Rebooting..."
  printf "\n\t  $(color r "REMOVE INSTALLATION MEDIA AFTER REBOOT")"
  sleep 15
  umount -R /mnt
  reboot
}

main() {
  printf "Starting $(color c "Arch") Installation Script...\n\n"
  verify_efi
  connectivity
  partitions
  create_vg
  create_lv
  format_vol
  mount_fs
  update_mirrors
  do_pacstrap
  gen_fstab
  do_chroot
  do_reboot
}

main "$@"
