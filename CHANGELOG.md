# arch-install Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## 0.2.0 (2018/07/26)
### Features
- Add SSD/NVME compatibility

### Modifications
 - **Removed**: [*post2*]()
 - [*install*]()
   - Added *null* function to replace `> /dev/null 2>&1`
   - Modified ESP checks for better consistency and SSD/NVME compatibility
   - Modified partitioning to avoid redundant checks
   - Modified partition variable assignments for better consistency
- [*chroot*]()
  - **Removed**: *create_swapfile* function
  - Moved functions *install_apps* and *install_utilities* from [*post*]() here
  - Added *create_user* function
  - Modified function *install_bootloader* to automatically detect Windows and add the ESP to */etc/fstab*
- [*post*]()
  - Moved functions from [*post2*]() here
  - Added extra dotfiles and font icons


## 0.1.0 (2018/07/10)
 - Initial release
